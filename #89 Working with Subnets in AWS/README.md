Your Python script snippet using Boto3 for listing all subnet IDs in your default AWS region, specifically those that are default for their availability zones, is almost ready to run. Before executing this script, ensure you have:

AWS CLI installed and configured on your machine. Boto3 uses the credentials configured for AWS CLI.

Boto3 installed in your Python environment. If not already installed, you can install it using pip:

```bash
pip install boto3
```

Here's the complete Python script based on your snippet:

```bash
import boto3

# Create an EC2 client
ec2 = boto3.client('ec2')

# Call describe_subnets to retrieve all subnets
subnets = ec2.describe_subnets()

# Loop through the subnets and print IDs of default subnets
for subnet in subnets["Subnets"]:
    if subnet["DefaultForAz"]:
        print(subnet["SubnetId"])
```

## Steps to Run the Script:
1. Save the script: Copy the script into a file named list_subnets.py.
2. Execute the script: Open a terminal or command prompt, navigate to the directory where list_subnets.py is saved, and run:

```bash
python list_subnets.py
```

This script will print the subnet IDs of all subnets that are marked as default for their respective availability zones in your default AWS region, as configured in your AWS CLI or Boto3 session settings.

## Additional Tips:
AWS Region: This script runs against your default AWS region. If you need to target a specific region, you can specify the region when creating the EC2 client:

```bash
ec2 = boto3.client('ec2', region_name='us-west-2')
```

Replace 'us-west-2' with your desired region.

AWS Credentials: Ensure your AWS credentials have sufficient permissions to call describe_subnets. The necessary permissions are included in many AWS-managed policies for EC2, but you can also create a custom policy with just the required permissions if needed.

By following these instructions, you should be able to successfully retrieve and print the IDs of default subnets in your AWS environment.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/automation-with-python/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
