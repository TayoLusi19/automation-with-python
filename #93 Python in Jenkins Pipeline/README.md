To create a Jenkins Pipeline that automates the deployment process from fetching Docker images from an ECR repository to deploying a selected image on an EC2 instance, you'll need to prepare your environment and write specific Python scripts and a Jenkinsfile. Here's a detailed guide on setting up and executing this pipeline.

## Preparation:
Ensure the EC2 instance is started, Docker and Python with necessary libraries (boto3, paramiko, requests) are installed on both Jenkins server and EC2 instance. Docker images tagged 1.0, 2.0, and 3.0 should be pushed to your ECR repository.

## Python Scripts:
get-images.py - Fetches image tags from ECR:

```bash
import boto3

ecr_client = boto3.client('ecr', region_name='AWS_DEFAULT_REGION')
response = ecr_client.describe_images(repositoryName='ECR_REPO_NAME')
image_tags = [detail['imageTags'][0] for detail in response['imageDetails']]
print("\n".join(image_tags))
```

deploy.py - Deploys the selected Docker image on EC2:

```bash
import paramiko
import sys

image_tag = sys.argv[1]  # Image tag passed as command line argument

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect('EC2_SERVER', username='ec2-user', key_filename='path/to/ssh-key.pem')

commands = [
    f"docker login -u AWS -p $(aws ecr get-login-password --region AWS_DEFAULT_REGION) ECR_REGISTRY",
    f"docker pull ECR_REGISTRY/ECR_REPO_NAME:{image_tag}",
    f"docker run -d -p HOST_PORT:CONTAINER_PORT ECR_REGISTRY/ECR_REPO_NAME:{image_tag}"
]

for command in commands:
    stdin, stdout, stderr = ssh.exec_command(command)
    print(stdout.read().decode())
    error = stderr.read().decode()
    if error:
        print(error)

ssh.close()
```

validate.py - Validates the application is running:

```bash
import requests
import sys

application_url = f"http://{sys.argv[1]}:{HOST_PORT}"

try:
    response = requests.get(application_url)
    if response.status_code == 200:
        print("Application is running successfully.")
    else:
        print("Application is not running as expected.")
except requests.ConnectionError:
    print("Failed to connect to the application.")
```

## Jenkinsfile:
Create a Jenkinsfile that orchestrates the pipeline:

```bash
pipeline {
    agent any
    environment {
        ECR_REPO_NAME = 'your-ecr-repo-name'
        EC2_SERVER = 'your-ec2-ip'
        ECR_REGISTRY = 'your-aws-id.dkr.ecr.your-region.amazonaws.com'
        CONTAINER_PORT = '80'
        HOST_PORT = '8080'
        AWS_DEFAULT_REGION = 'your-region'
    }
    stages {
        stage('Fetch Images') {
            steps {
                script {
                    images = sh(script: "python3 get-images.py", returnStdout: true).trim()
                    IMAGES_LIST = images.split("\n")
                }
            }
        }
        stage('Select Image and Deploy') {
            steps {
                script {
                    CHOSEN_IMAGE = input message: 'Choose an image to deploy', parameters: [choice(name: 'IMAGE_TAG', choices: IMAGES_LIST.join('\n'), description: 'Select an image tag')]
                    sh "python3 deploy.py ${CHOSEN_IMAGE}"
                }
            }
        }
        stage('Validate Deployment') {
            steps {
                sh "python3 validate.py ${EC2_SERVER}"
            }
        }
    }
}
```

## Manual Steps:
- Install Python and necessary packages on the Jenkins server.
- Configure AWS credentials and SSH key as Jenkins credentials.
- Ensure your Jenkins has permissions to approve script methods like split if sandbox security is enabled.

## Execution:
1. Commit the Python scripts to your repository.
2. Set up a new Jenkins Pipeline job and point it to your repository.
3. Run the pipeline - During execution, you'll select the Docker image to deploy. The pipeline will then deploy the selected image on your EC2 instance and validate the deployment.

This Jenkins Pipeline provides a structured approach to automate deployment workflows, leveraging Python for AWS interactions and deployment tasks. Make sure to replace placeholder values with actual data from your AWS setup.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/automation-with-python/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
