This project will automate the deployment and monitoring of an Nginx application on an AWS EC2 instance using Python, we'll streamline the approach while addressing the prerequisites and security concerns mentioned. This solution will cover:

1. Automatically creating an EC2 instance and configuring it to run Nginx inside Docker.
2. Monitoring the Nginx application and implementing a restart mechanism upon failure.

Prerequisites:
- AWS CLI and Boto3 installed and configured with appropriate permissions.
- Paramiko library installed for SSH operations: pip install paramiko.
- Requests library installed for HTTP requests: pip install requests.
- Schedule library installed for scheduling tasks: pip install schedule.
- An existing key pair in AWS and the corresponding private key file available locally.
- The default security group configured to allow SSH and HTTP traffic.
Python Script:

```bash
import boto3
import paramiko
import requests
import schedule
import time

ec2_resource = boto3.resource('ec2')
ec2_client = boto3.client('ec2')

# Configuration variables
image_id = 'ami-031eb8d942193d84f'  # Example AMI, update with a valid one
key_name = 'your-key-pair-name'
instance_type = 't2.small'
ssh_private_key_path = '/path/to/your/private/key.pem'
ssh_user = 'ec2-user'

# Create EC2 instance
instance = ec2_resource.create_instances(
    ImageId=image_id,
    KeyName=key_name,
    MinCount=1,
    MaxCount=1,
    InstanceType=instance_type,
    TagSpecifications=[{'ResourceType': 'instance', 'Tags': [{'Key': 'Name', 'Value': 'my-server'}]}],
)[0]

# Wait for the instance to be in a running state
instance.wait_until_running()
instance.reload()

print(f"Instance {instance.id} is running")

# Install Docker and run Nginx container
def setup_docker_and_nginx(host_ip):
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=host_ip, username=ssh_user, key_filename=ssh_private_key_path)

    commands = [
        'sudo yum update -y',
        'sudo amazon-linux-extras install docker -y',
        'sudo systemctl start docker',
        'sudo systemctl enable docker',
        'sudo docker run -d -p 80:80 nginx'
    ]

    for command in commands:
        stdin, stdout, stderr = ssh_client.exec_command(command)
        print(stdout.read().decode())
        err = stderr.read().decode()
        if err:
            print(err)

    ssh_client.close()

# Open port 80 in the default security group
def open_port_80():
    security_group = ec2_resource.SecurityGroup(instance.security_groups[0]['GroupId'])
    try:
        security_group.authorize_ingress(
            CidrIp='0.0.0.0/0',
            IpProtocol='tcp',
            FromPort=80,
            ToPort=80
        )
    except Exception as e:
        print(e)

open_port_80()
setup_docker_and_nginx(instance.public_ip_address)

# Monitoring and restart mechanism
def monitor_and_restart():
    global failure_count
    try:
        response = requests.get(f"http://{instance.public_ip_address}")
        if response.status_code == 200:
            print("Nginx is running successfully")
            failure_count = 0
        else:
            print("Nginx is not OK")
            failure_count += 1
    except requests.ConnectionError:
        print("Failed to connect to Nginx")
        failure_count += 1

    if failure_count >= 5:
        print("Restarting Nginx container...")
        setup_docker_and_nginx(instance.public_ip_address)

failure_count = 0
schedule.every(1).minutes.do(monitor_and_restart)

while True:
    schedule.run_pending()
    time.sleep(1)
```

## Key Points:
- AMI ID: Ensure the image_id variable is set to a valid AMI that supports Docker, such as Amazon Linux 2.
- Key Pair: The key_name and ssh_private_key_path must match your existing AWS key pair.
- Security Group Configuration: This script attempts to open port 80 for HTTP traffic. Ensure your AWS account allows modifying security group rules programmatically.
- Monitoring: The script uses a simple HTTP request to check if Nginx is running. If it fails 5 times consecutively, it attempts to restart the Nginx Docker container.

This script automates the creation of an EC2 instance, setting up Docker and Nginx, and provides a basic monitoring and restart mechanism. Ensure to test and adapt it according to your specific AWS setup and security requirements.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/automation-with-python/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
