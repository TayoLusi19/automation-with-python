Your Python script for listing all IAM users in your AWS account and identifying the most recently active user can be improved for better error handling and to account for users who may not have used their password. Here's an updated version of your script with enhancements:

```bash
import boto3
from datetime import datetime

# Create an IAM client
iam = boto3.client('iam')

# List all IAM users
iam_users = iam.list_users()

# Initialize variables to track the most recently active user
last_active_user = None
last_active_time = datetime.min

print("All IAM Users and Last Password Usage:")
print("---------------------------------------")

for iam_user in iam_users["Users"]:
    user_name = iam_user["UserName"]
    # Some users might not have 'PasswordLastUsed' attribute if they haven't logged in
    password_last_used = iam_user.get("PasswordLastUsed", "Never used")
    
    print(f"User Name: {user_name}")
    print(f"Password Last Used: {password_last_used}")
    print("---------------------------")
    
    # Update the most recently active user
    if password_last_used != "Never used" and password_last_used > last_active_time:
        last_active_user = iam_user
        last_active_time = password_last_used

if last_active_user:
    print("Most Recently Active User:")
    print(f"User ID: {last_active_user['UserId']}")
    print(f"User Name: {last_active_user['UserName']}")
    print(f"Password Last Used: {last_active_user['PasswordLastUsed']}")
else:
    print("No users have logged in with a password.")
```

## Enhancements and Notes:
- Error Handling for Users Without PasswordLastUsed: This script now checks if the PasswordLastUsed attribute exists for each user. If a user has never logged in, this attribute won't be present. The script handles this case by assigning "Never used" for users without this attribute.
- Initialization of last_active_time: Initializes last_active_time with datetime.min to ensure comparison logic works correctly from the start.
- Final Print Statement: The script concludes by printing details of the most recently active user, if any user has logged in using a password.

## Running the Script:
Save the script in a file, e.g., list_iam_users.py.
Ensure you have AWS credentials configured that grant permission to list IAM users. This typically requires permissions like iam:ListUsers and iam:GetUser.
Run the script using a Python interpreter:

```bash
python list_iam_users.py
```

## Additional Considerations:
- Permissions: Running this script requires AWS credentials with permissions to list IAM users and access their details.
- Pagination: If your account has a large number of IAM users, you might need to handle pagination in the list_users call to ensure you retrieve all users.
- Security Best Practices: Be cautious when handling and displaying sensitive information, such as login activity, to ensure you're complying with your organization's security policies.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/automation-with-python/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi
